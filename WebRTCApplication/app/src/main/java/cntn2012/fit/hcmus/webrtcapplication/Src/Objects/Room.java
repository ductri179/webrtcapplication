package cntn2012.fit.hcmus.webrtcapplication.Src.Objects;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NQH on 4/23/2016.
 */
public class Room {
    private User user;

    private List<Message> msgList;

    public Room(User user) {
        this.user = user;
        msgList= new ArrayList<>();
    }

    public void addMsg(Message msg){
        msgList.add(msg);
    }
    public List<Message> getMsgList() {
        return msgList;
    }

    public User getUser() {
        return user;
    }




}
