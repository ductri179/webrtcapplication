package cntn2012.fit.hcmus.webrtcapplication.Src.Adapters;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cntn2012.fit.hcmus.webrtcapplication.R;
import cntn2012.fit.hcmus.webrtcapplication.Src.Global.Global;
import cntn2012.fit.hcmus.webrtcapplication.Src.IOSocket.SocketIOAccess;
import cntn2012.fit.hcmus.webrtcapplication.Src.IOSocket.SocketIOMessage;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.FriendRequest;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.Message;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.UsersObjectsManager;

/**
 * Created by NQH on 4/17/2016.
 */
public class FriendRequestListAdapter extends ArrayAdapter implements View.OnClickListener {
    Context context;
    List<FriendRequest> frList;
    Button btnAccept;
    Button btnDecline;
    public FriendRequestListAdapter(Context context, int resource, List<FriendRequest> frList) {
        super(context, resource, frList);
        this.context = context;
        this.frList = frList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        FriendRequest fr = frList.get(position);


        View rowView = inflater.inflate(R.layout.list_item_friend_request, parent, false);
        TextView tvUserName = (TextView) rowView.findViewById(R.id.tvFriendRequestUserName);
        ImageView iv = (ImageView) rowView.findViewById(R.id.imgFriendRequestUserAvatar);
        btnAccept = (Button) rowView.findViewById(R.id.btnFrientRequestAccept);
        btnDecline = (Button) rowView.findViewById(R.id.btnFrientRequestDecline);
        tvUserName.setText(fr.getFrom());
        btnAccept.setTag(position);
        btnDecline.setTag(position);
        btnAccept.setOnClickListener(this);
        btnDecline.setOnClickListener(this);



        return rowView;
    }

    @Override
    public void onClick(View v) {
        String user = frList.get((int)v.getTag()).getFrom();
        SocketIOMessage msg = new SocketIOMessage(Global.SOCKET_MESSAGE_RESPONSE_FRIEND_REQUEST, UsersObjectsManager.getInstance().getUser().userName, user);
        if(v==btnAccept)
            msg.addArgument("response","accept");
        else
            msg.addArgument("response","decline");
        frList.remove((int)v.getTag());
        notifyDataSetChanged();
        SocketIOAccess.getInstance().emitMessage(msg);
    }
}
