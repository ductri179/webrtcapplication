package cntn2012.fit.hcmus.webrtcapplication.Src.Objects;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NQH on 4/23/2016.
 */
public class RoomManager {

    private static final RoomManager instance= new RoomManager();
    public static RoomManager getInstance(){
        return instance;
    }
    private RoomManager(){
        listRoom= new ArrayList<>();
    }

    public List<Room> getListRoom() {
        return listRoom;
    }

    private List<Room> listRoom;

    public Room getRoom(String username) {
        for (int i = 0, length = listRoom.size(); i < length; ++i) {
            if (listRoom.get(i).getUser().userName.equals(username))
                return listRoom.get(i);
        }
        User user=new User();  //server.getUserInfo;
        user.userName=username;
        Room room =new Room(user);
        listRoom.add(room);
        return room;
    }

    public  void addRoom(Room room){
        if(room!=null && getRoom(room.getUser().userName)==null)
            listRoom.add(room);
    }

    public void removeRoom(String username){
        for (int i = 0, length = listRoom.size(); i < length; ++i) {
            if (listRoom.get(i).getUser().userName == username) {
                listRoom.remove(i);
                return;
            }
        }
    }
}
