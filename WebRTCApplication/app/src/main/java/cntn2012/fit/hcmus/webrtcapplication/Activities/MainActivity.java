package cntn2012.fit.hcmus.webrtcapplication.Activities;

import android.app.Activity;
import android.app.TabActivity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.ViewParent;
import android.widget.FrameLayout;
import android.widget.TabHost;
import android.widget.TableLayout;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;

import org.json.JSONException;
import org.json.JSONObject;

import cntn2012.fit.hcmus.webrtcapplication.Activities.PagerAdapter.MyPagerAdapter;
import cntn2012.fit.hcmus.webrtcapplication.Activities.Tabs.FriendListFragment;
import cntn2012.fit.hcmus.webrtcapplication.Activities.Tabs.HomeFragment;
import cntn2012.fit.hcmus.webrtcapplication.Activities.Tabs.ProfileFragment;
import cntn2012.fit.hcmus.webrtcapplication.Activities.Tabs.RoomFragment;
import cntn2012.fit.hcmus.webrtcapplication.R;
import cntn2012.fit.hcmus.webrtcapplication.Src.Global.Global;
import cntn2012.fit.hcmus.webrtcapplication.Src.IOSocket.SocketIOAccess;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.UsersObjectsManager;
import cntn2012.fit.hcmus.webrtcapplication.Src.WebRTC.WebRTCConfig;

public class MainActivity extends AppCompatActivity {

    SocketIOAccess socket;
    UsersObjectsManager uoManager=UsersObjectsManager.getInstance();
    Toast toast;

    TabLayout tabLayout;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initTabsLayout();
        toast= Toast.makeText(getBaseContext(),"", Toast.LENGTH_SHORT);

        socket=SocketIOAccess.getInstance();
        socket.on(Global.SOCKET_EVENT_MESSAGE_LISTENER, onMessageListener);

    }

    private Emitter.Listener onMessageListener = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String messageType;
                    String from;
                    try {
                        messageType=data.getString("type");
                        from=data.getString("from");

                    } catch (JSONException e) {
                        messageType="";
                        from="";
                    }
                    switch(messageType)
                    {
                        default:
                            break;
                    }
                }
            });
        }
    };



    private void initTabsLayout() {
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        PagerAdapter adapter = new MyPagerAdapter(getSupportFragmentManager(), 4);
        viewPager.setAdapter(adapter);

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);

        int[] iconIds = {
                R.drawable.tab_home,
                R.drawable.tab_friends,
                R.drawable.tab_room,
                R.drawable.tab_profile
        };

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            Drawable icon = getResources().getDrawable(iconIds[i]);
            Bitmap bitmap = ((BitmapDrawable) icon).getBitmap();
            Drawable icon1 = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 60, 60, true));
            if (i != 0)
                icon1.setAlpha(50);
            tab.setIcon(icon1);
        }

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                tab.getIcon().setAlpha(250);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tab.getIcon().setAlpha(50);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        WebRTCConfig.client.disconnect();
    }
}
