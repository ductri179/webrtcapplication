/**
 *  create by NQH
 */
var assert = require('assert');

var http = require('http');
var socketIO = require('socket.io');
var server = http.createServer();



var port = process.env.PORT || 1000;
var ip = process.env.IP || '127.0.0.1';


//========Data ===============================================
var MongoClient=require('mongodb').MongoClient;
var assert = require('assert');
// Connection db URL
//var dbUrl = 'mongodb://localhost:27017/eclearning';
var dbUrl = 'mongodb://admin:qweqwe@ds025449.mlab.com:25449/eclearning'


var usersCollection;

MongoClient.connect(dbUrl, function(err, db) {
  if(err)
  	console.log('Can not connect to server');
  else
  	console.log("Connected  to server");

  usersCollection =db.collection('users');
  userdataCollection=db.collection('UserData');
});





//===========================Server===========================================

server.listen(port, function(){
	console.log('Server has started at localhost: %s | port: %s', ip, port);
});

var socketList=[];
var io = socketIO.listen(server);
io.sockets.on('connection', function(socket){
	
	console.log('client da ket noi:' + socket.id);

	socket.on('login',function(from,to,data){
		var user= {'username': data.username, 'password': data.password};
		usersCollection.find(user).toArray(function(err,doc){
			if(err)
			{
				socket.emit('login',0);
				console.log('Error: database err ');
			}
			else if(doc.length==0){
				socket.emit('login',2);
				console.log('Login failed: '+data.username + ' ');
			}else{
		 		socket.emit('login',1);
				console.log('Login: '+data.username);
				socket.username=data.username;
				socketList.push(socket);
				getQueueMessage(data.username,function(QueueMessage){
					QueueMessage.forEach(function(msg){
						switch(msg.type){
							case 'friend_request':
								socket.emit('friend_message',msg);
							break;
							case 'private_message':
								socket.emit('private_message',msg);
								deleteQueueMessage(data.username, msg.from, 'private_message');
							break;
						}
						
					});
				});

				getFriendList(data.username, function(friends){
					var msg= new Object();
					msg['type']='friend_list';
					msg['from']='';
					msg['to']='';
					friends.forEach(function(friend){
						msg['data']=friend;
						socket.emit('friend_message',msg);
					});
				});

			}
		});
	});

	socket.on('register',function(from,to,data){
		console.log('Register:')
		console.log(data);
		var user = {'username': data.username, 'password': data.password};
		usersCollection.find(user).toArray(function(err,doc){
			if(err)
			{
				socket.emit('register',0);
				console.log('Error: database err ');
			}
			else if(doc.length==0){
				var userdata = createNewUserData(data.username);
				usersCollection.insertOne(user);
				userdataCollection.insertOne(userdata);
				socket.emit('register',1);
				console.log('Register successfully: '+data.username + ' ');
			}else{
		 		socket.emit('register',2);
				console.log('Register failed: '+data.username);
			}});
		});

	socket.on('friend_request',function(from,to,data){
		var user = {$and:[{'username': to.username}, {'username':{$ne: from.username}}]};

		usersCollection.find(user).toArray(function(err,doc){
			if(err)
			{
				//socket.emit('socket_message',doc.);
				console.log('Error: database err ');
			}
			else if(doc.length==0){
				var msg=new Object();
				msg['type']="response_friend_request"
				msg['from']=from.username;
				msg['to']=to.username;
				msg['data']='non_exist';
				socket.emit('friend_message',msg);
				console.log('search friend: '+from.username+ '-- request add friend: ' +to.username + ' failed ');
				
			}else{
				checkExistFriend(from.username,to.username, function(result1){
					if(!result1){
						checkExistQueueMessage(to.username, from.username, 'friend_request', function(result2){
							if(!result2){
								var msg= new Object();
								msg['type']="friend_request";
								msg['from']=from.username;
								msg['to']=to.username;
								saveQueueMessage(to.username, msg);
								for(var i=0, length=socketList.length; i<length; ++i){
									if(socketList[i].username==to.username){
										socketList[i].emit('friend_message',msg);
										console.log('friend request: '+from.username+ ' -- sent to : ' +to.username );
										break;
									}
								}
							}
							else{
								var msg=new Object();
								msg['type']="response_friend_request"
								msg['from']=from.username;
								msg['to']=to.username;
								msg['data']='waiting';
								socket.emit('friend_message',msg);
								console.log('search friend: '+from.username+ '-- request add friend: ' +to.username + ' waiting ');
							}
						});

					}else{
							var msg=new Object();
							msg['type']="response_friend_request"
							msg['from']=from.username;
							msg['to']=to.username;
							msg['data']='already_a_friend';
							socket.emit('friend_message',msg);
							console.log('search friend: '+from.username+ '-- request add friend: ' +to.username + ' already a friend ');
						}
				});
				
			}				
		});
	});

	socket.on('response_friend_request',function(from,to,data){
		console.log('friend request response: '+from.username+ ' -- response : ' +to.username);
		var user = {$and:[{'username': to.username}, {'username':{$ne: from.username}}]};
		var k=data.response;
		console.log(k);
		if(k=='accept'){
			saveNewFriend(from.username, to.username);
			saveNewFriend(to.username, from.username);
			
			var msg= new Object();
			msg['type']='friend_list';
			msg['from']='';
			msg['to']='';
			socketList.forEach(function(s){
				if(s.username==from.username){
					msg['data']={'username': to.username };
					s.emit('friend_message',msg);
				}
				if(s.username==to.username){
					msg['data']={'username': from.username };
					s.emit('friend_message',msg);
				}
			});
		}

		deleteQueueMessage(from.username, to.username, 'friend_request');

	});

	socket.on('private_message',function(from, to, data){
		var user = {'username': to.username};
		
		var msg= new Object();
		msg['type']='private_message';
		msg['from']=from.username;
		msg['to']=to.username;
		msg['content']=data.content;
		msg['time']=data.time;
		console.log(msg);
		var i=0, length=socketList.length;

		for(;i<length; ++i){
			if(socketList[i].username==to.username){
				socketList[i].emit('private_message',msg);
				break;
			}
		}
		if(i==length)
			saveQueueMessage(to.username, msg);

		

	});

	socket.on('disconnect',function(){

		if (socket.hasOwnProperty('username')){
			console.log('user ngat ket noi:' + socket.username);
			var i=socketList.indexOf(socket);
			if(i!= -1){
				socketList.splice(i,1);
				console.log('so socket: '+ socketList.length);
			
			}
		}					
		

	});
});



function createNewUserData(username){
	userdata= new Object();
	userdata['username']=username;
	userdata['QueueMessage']=[];
	userdata['Friends']=[];

	return userdata;
}

function saveQueueMessage(username, msg, type){

	var user={'username': username};
	var userdata;
	userdataCollection.find(user).toArray(function(err,doc){
		if(err){
			console.log('save queue message error');
		}
		else{
			if(doc.length==0){
				userdata= createNewUserData(username);
				userdata.QueueMessage.push(msg);
				userdataCollection.insertOne(userdata);

			}
			else{
				userdata=doc[0];
				userdata.QueueMessage.push(msg);
				userdataCollection.updateOne(user,{$set:{'QueueMessage':userdata.QueueMessage}});

			}
		}
	});
}


function getQueueMessage(username,callback){
	var user={'username': username};
	var userdata;
	userdataCollection.find(user).toArray(function(err,doc){
		if(err){
			console.log('save queue message error');
		}
		else{
			if(doc.length>0){
				queue=doc[0].QueueMessage;
				callback(queue);
			}
		}
	});
}

function deleteQueueMessage(user, from, type){
	userdataCollection.find({'username': user}).toArray(function(err,doc){
		queueMsg=doc[0].QueueMessage;
		for(var i=0, length=queueMsg.length; i<length; ++i){
			if(queueMsg[i].type==type && queueMsg[i].from==from){
				// delete queueMsg[i];
				queueMsg.splice(i,1);
				userdataCollection.updateOne({'username': user}, {$set:{'QueueMessage':queueMsg}});
				break;
			}
		}			
	});
}

function checkExistQueueMessage(user, from, type, callback){
	userdataCollection.find({'username': user}).toArray(function(err,doc){
		if(err || doc.length==0)
		{
			callback(false);
		}
		else{

			queueMsg=doc[0].QueueMessage;
			var i=0, length=queueMsg.length;
			for(; i<length; ++i){
				if(queueMsg[i].type==type && queueMsg[i].from==from){
					callback(true);
					break;
				}
			}
			if(i==length){
				callback(false);
			}			
		}
	});
}
function checkExistFriend(user, friend, callback){
	userdataCollection.find({'username': user}).toArray(function(err,doc){
		if(doc.length==0)
		{
			callback(false);
		}
		else{
			var Friends=doc[0].Friends;
			var i=0, length=Friends.length;
			for(; i<length; ++i){
				if(Friends[i].username==friend){
					callback(true);
					break;
				}
			}
			if(i==length){
				callback(false);
			}			
		}
	});
}


function saveNewFriend(user, friendUsername){
	var userdata;
	userdataCollection.find({'username': user}).toArray(function(err,doc){
				if(doc.length==0){
					userdata=createNewUserData(user);
					userdata.Friends.push(friendUsername);
					userdataCollection.insertOne(userdata);

				}
				else{
					userdata=doc[0];
					userdata.Friends.push({'username': friendUsername});
					userdataCollection.updateOne({'username':user},{$set:{'Friends':userdata.Friends}});

				}
			});
}

function getFriendList(username,callback){
	var user={'username': username};
	var userdata;
	userdataCollection.find(user).toArray(function(err,doc){
		if(err){
			console.log('save queue message error');
		}
		else{
			if(doc.length>0){
				callback(doc[0].Friends);
			}
		}
	});
}



function deleteQueueChatMessage(user, from){
	userdataCollection.find({'username': user}).toArray(function(err,doc){
		userdata=doc[0];
		userdata.QueueMessage.deleteOne({'from':from, 'type':'type'}, function(err,result){});		
	});
}