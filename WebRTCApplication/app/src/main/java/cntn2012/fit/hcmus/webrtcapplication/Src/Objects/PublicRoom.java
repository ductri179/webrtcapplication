package cntn2012.fit.hcmus.webrtcapplication.Src.Objects;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NQH on 4/23/2016.
 */
public class PublicRoom {

    private User master;
    List<User> members;
    int max_number_of_members=4;
    private String roomTitle;
    private List<Message> msgList;

    public PublicRoom(User master, String roomTitle) {
        this.roomTitle=roomTitle;
        this.master = master;
        List<User> members= new ArrayList<>();
        msgList= new ArrayList<>();
    }

    public void addMsg(Message msg){
        msgList.add(msg);
    }
    public List<Message> getMsgList() {
        return msgList;
    }

    public User getMaster() {
        return master;
    }
    public String getRoomTitle(){
        return roomTitle;
    }
    public boolean addMember(User user) {

        if(members.size()>= max_number_of_members)
            return false;
        if(user.userName.equals(master.userName))
            return false;
        for(User u : members){
           if(u.userName.equals(master.userName))
               return false;
        }

        members.add(user);
        return true;

    }
    public int getNumberOfMembers(){
        return members.size()+1;
    }
    public boolean removeMember(String username) {
        if (master.userName.equals(username))
            if (members.size() == 0)
                return false;
            else {
                master = members.get(0);
                members.remove(0);
            }
        else{
            for (User u : members){
                if(u.userName.equals(username)) {
                    members.remove(u);
                    return true;
                }
            }
        }
        return false;
    }




}
