package cntn2012.fit.hcmus.webrtcapplication.Activities.PagerAdapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import cntn2012.fit.hcmus.webrtcapplication.Activities.Tabs.FriendListFragment;
import cntn2012.fit.hcmus.webrtcapplication.Activities.Tabs.HomeFragment;
import cntn2012.fit.hcmus.webrtcapplication.Activities.Tabs.ProfileFragment;
import cntn2012.fit.hcmus.webrtcapplication.Activities.Tabs.RoomFragment;

/**
 * Created by ductri on 4/30/16.
 */
public class MyPagerAdapter extends FragmentStatePagerAdapter {

    int tabCount;
    Fragment[] fragments = {new HomeFragment(), new FriendListFragment(), new RoomFragment(), new ProfileFragment()};

    public MyPagerAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
}

    @Override
    public int getCount() {
        return tabCount;
    }
}
