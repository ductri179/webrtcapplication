package cntn2012.fit.hcmus.webrtcapplication.Activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;

import cntn2012.fit.hcmus.webrtcapplication.R;
import cntn2012.fit.hcmus.webrtcapplication.Src.Functions.UsernameAndPasswordChecker;
import cntn2012.fit.hcmus.webrtcapplication.Src.Global.Global;
import cntn2012.fit.hcmus.webrtcapplication.Src.IOSocket.SocketIOAccess;
import cntn2012.fit.hcmus.webrtcapplication.Src.IOSocket.SocketIOMessage;

public class SignUpActivity extends Activity implements View.OnClickListener{

    SocketIOAccess socket;

    Button btnSignUp;
    EditText etUsername;
    EditText etPassword;
    EditText etRetypePassword;
    TextView tvLogin;
    TextView tvUsernameError;
    TextView tvPasswordError;
    TextView tvRetypePasswordError;

    Toast toast;
    UsernameAndPasswordChecker stringChecker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        toast =Toast.makeText(getBaseContext(),"",Toast.LENGTH_SHORT);
        stringChecker= new UsernameAndPasswordChecker();
        //socket=SocketIOAccess.getInstance();
        socket= SocketIOAccess.getInstance();
        socket.connect();
        connectView();
        setEvents();
    }

    private void connectView() {
        btnSignUp=(Button)findViewById(R.id.btnSignUp);
        etUsername=(EditText)findViewById(R.id.etRegisterUsername);
        etPassword=(EditText)findViewById(R.id.etRegisterPassword);
        etRetypePassword=(EditText)findViewById(R.id.etRegisterRetypePassword);
        tvLogin=(TextView)findViewById(R.id.tvLogIn);
        tvUsernameError=(TextView)findViewById(R.id.tvRegisterUsernameError);
        tvPasswordError=(TextView)findViewById(R.id.tvRegisterPasswordError);
        tvRetypePasswordError=(TextView)findViewById(R.id.tvRegisterRetypePasswordError);
    }
    private void setEvents() {
        btnSignUp.setOnClickListener(this);
        tvLogin.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if(v==btnSignUp){
            String strUsername=etUsername.getText().toString();
            String strPassword=etPassword.getText().toString();
            String strRetypePassword=etRetypePassword.getText().toString();

            tvUsernameError.setText(stringChecker.checkValidString(strUsername));
            tvPasswordError.setText(stringChecker.checkValidString(strPassword));

            if(!strPassword.equals(strRetypePassword))
                tvRetypePasswordError.setText(R.string.RegisterError2);
            else
                tvRetypePasswordError.setText("");

            if(tvUsernameError.getText().equals("") && tvUsernameError.getText().equals("") && tvRetypePasswordError.getText().equals("")){
                SocketIOMessage msg=new SocketIOMessage(Global.SOCKET_EVENT_REGISTER,"","");
                msg.addArgument("username", strUsername);
                msg.addArgument("password", strPassword);

                socket.emitMessage(msg);
                socket.on(Global.SOCKET_EVENT_REGISTER, onRegisterListener);

            }


        }else if(v==tvLogin)
            finish();
    }

    private Emitter.Listener onRegisterListener = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    int result  = (int) args[0];
                    switch(result)
                    {
                        case 0:
                            toast.setText(R.string.RegisterError1);
                            toast.show();
                            break;
                        case 1:
                            toast.setText(R.string.RegisterString1);
                            toast.show();
                            socket.off("new message", onRegisterListener);
                            finish();
                            break;
                        default:
                            toast.setText(R.string.RegisterString2);
                            toast.show();
                            break;
                    }
                }
            });
        }
    };
}
