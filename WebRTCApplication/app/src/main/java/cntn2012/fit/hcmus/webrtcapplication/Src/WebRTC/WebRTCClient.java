package cntn2012.fit.hcmus.webrtcapplication.Src.WebRTC;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.AudioSource;
import org.webrtc.AudioTrack;
import org.webrtc.DataChannel;
import org.webrtc.IceCandidate;
import org.webrtc.MediaConstraints;
import org.webrtc.MediaSource;
import org.webrtc.MediaStream;
import org.webrtc.PeerConnection;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.SdpObserver;
import org.webrtc.SessionDescription;
import org.webrtc.VideoCapturer;
import org.webrtc.VideoCapturerAndroid;
import org.webrtc.VideoSource;
import org.webrtc.VideoTrack;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.User;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.UsersObjectsManager;

/**
 * Created by Oscar on 3/28/2016.
 */
public class WebRTCClient {
    private PeerConnectionFactory pcFactory;
    private LinkedList<PeerConnection.IceServer> iceServers = new LinkedList<PeerConnection.IceServer>();
    private MediaConstraints pcConstraints = new MediaConstraints();
    private Socket socketClient;
    private HashMap<String, Peer> peers;
    private MediaStream localMS;
    private RtcListener mListener;
    private VideoSource videoSource;
    private AudioSource audioSource;
    private AudioTrack audioTrack;
    private VideoTrack videoTrack;

    public WebRTCClient(String host, final Context context) {
        PeerConnectionFactory.initializeAndroidGlobals(context, true, true, true, null);
        pcFactory = new PeerConnectionFactory();
        peers = new HashMap<String, Peer>();
        addIceServer();

        try {
            socketClient = IO.socket(host);
            socketClient.connect();
            socketClient.on(WebRTCConstants.MESSAGE, new onMessageFromServer());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        //pcConstraints.mandatory.add(new MediaConstraints.KeyValuePair("OfferToReceiveAudio", "false"));
        pcConstraints.mandatory.add(new MediaConstraints.KeyValuePair("OfferToReceiveVideo", "true"));
        pcConstraints.optional.add(new MediaConstraints.KeyValuePair("DtlsSrtpKeyAgreement", "true"));
    }

    public interface RtcListener{
        void onLocalStream(MediaStream localStream);
        void onRemoteStream(MediaStream remoteStream, String peerId);
        void onOffer(String from);
        void onReceiveMessage(DataChannel.Buffer buffer, String peerId);
        void onNewUserJoin(String userName);
        void onSetRemoteSdpSuccessful(String userName);
    }

    public void setListener(RtcListener listener) {
        this.mListener = listener;
    }

    //Connect controls
    public void disconnect() {

        if(localMS!=null)
            localMS .dispose();
//        if (videoSource != null && videoSource.state() == MediaSource.State.LIVE) {
//            Log.d(WebRTCConstants.LOG_TAG, "Stopping video source");
//            videoSource.stop();
//        }
//        if (audioSource != null && audioSource.state() == MediaSource.State.LIVE) {
//            Log.d(WebRTCConstants.LOG_TAG, "Stopping audio source");
//            audioSource.
//        }
//
        for (Map.Entry<String, Peer> entry : peers.entrySet()) {
            Log.d(WebRTCConstants.LOG_TAG, "Disconnect to peers");
            Peer peer = entry.getValue();
            removePeer(peer.id);
        }
    }

    public void pause() {
        if (videoSource != null && videoSource.state() == MediaSource.State.LIVE) {
            Log.d(WebRTCConstants.LOG_TAG, "Stopping video source");
            videoSource.stop();
        }
        if (audioSource != null && audioSource.state() == MediaSource.State.LIVE) {
            Log.d(WebRTCConstants.LOG_TAG, "Stopping audio source");
            audioSource.dispose();
        }
    }

    public void resume() {
        if (videoSource != null && videoSource.state() == MediaSource.State.LIVE) {
            videoSource.restart();
        }
    }

    //------------>LISTENERS
    private class MyDataObserver implements DataChannel.Observer {
        String peerId;

        public MyDataObserver(String peerId) {
            this.peerId = peerId;
        }

        @Override
        public void onBufferedAmountChange(long l) {
           Log.d(WebRTCConstants.LOG_TAG, "On buffered amount change: " + l);
        }

        @Override
        public void onStateChange() {
           Log.d(WebRTCConstants.LOG_TAG, "On state change");
        }

        @Override
        public void onMessage(DataChannel.Buffer buffer) {
            Log.d(WebRTCConstants.LOG_TAG, "On data channel message: " + ((buffer.binary == true)?"binary" : "not binary"));
            mListener.onReceiveMessage(buffer, peerId);
        }
    }

    private class onMessageFromServer implements Emitter.Listener {
        @Override
        public void call(Object... args) {
            JSONObject data = (JSONObject) args[0];
            try {
                String type = data.getString("type");
                String from = data.getString("from");
                JSONObject payload = data.getJSONObject("payload");
                if (type.equals(WebRTCConstants.CANDIDATE)) {
                    addIceCandidate(from, payload);
                } else if (type.equals(WebRTCConstants.OFFER)) {
                    mListener.onOffer(from);
                    setRemoteDescription(from, payload);
                } else if (type.equals(WebRTCConstants.ANSWER)) {
                    setRemoteDescription(from, payload);
                } else if (type.equals(WebRTCConstants.NEW_USER_JOIN_ROOM)) {
                    Log.d(WebRTCConstants.LOG_TAG, "New user join room: " + from);
                    mListener.onNewUserJoin(from);
                    makeConnection2NewUser(from);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    //-------->ACTIONS
    @Nullable
    private Peer addPeer(String from) {
        Peer peer;
        if (peers.size() < WebRTCConfig.MAX_CLIENTS) {
            peer = new Peer(from);
            peers.put(from, peer);
        } else {
            Log.d(WebRTCConstants.LOG_TAG, "Max client size is exceeded");
            return null;
        }
        return peer;
    }

    private void removePeer(String id) {
        Peer peer = peers.get(id);
        peer.pc.close();
        peers.remove(peer.id);
    }

    private void setRemoteDescription(String from, JSONObject payload) {
        try {
            Peer peer = peers.get(from);
            if (peer == null) {
                peer = addPeer(from);
                if (peer == null) return;
            }
            SessionDescription sdp = new SessionDescription(
                    SessionDescription.Type.fromCanonicalForm(payload.getString("type")),
                    payload.getString("sdp")
            );
            peer.pc.setRemoteDescription(peer, sdp);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void addIceCandidate(String from, JSONObject payload) {
        try {
            Peer peer = peers.get(from);
            if (peer == null) {
                peer = addPeer(from);
                if (peer == null) return;
            }
            if (peer.pc.getRemoteDescription() != null) {
                IceCandidate candidate =  new IceCandidate(
                        payload.getString("id"),
                        payload.getInt("index"),
                        payload.getString("sdp")
                );
                Log.d(WebRTCConstants.LOG_TAG, "Adding Ice candidate");
                peer.pc.addIceCandidate(candidate);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void registerToSignalingServer(String userName) {
        try {
            JSONObject message = new JSONObject();
            message.put("userId", userName);
            socketClient.emit("register", message);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void makeConnection2NewUser(String from) {
        Peer peer = peers.get(from);
        if (peer == null) {
            peer = addPeer(from);
            if (peer == null) return;
        }
        peer.pc.createOffer(peer, pcConstraints);
    }

    public void makeCall(String peerId) {
        Peer peer = peers.get(peerId);
        if (peer != null) {
            setMedia();
            peer.pc.addStream(localMS);
            peer.pc.createOffer(peer, pcConstraints);
            mListener.onLocalStream(localMS);
        } else {
            Log.d(WebRTCConstants.LOG_TAG, "Peer id is not exists");
        }
    }

    public void makeAnswer(String peerId) {
        Peer peer = peers.get(peerId);
        if (peer != null) {
            //setMedia();
            //peer.pc.addStream(localMS);
            peer.pc.createAnswer(peer, pcConstraints);
        } else {
            Log.d(WebRTCConstants.LOG_TAG, "Peer id is not exists");
        }
    }

    public void joinRoom(String roomName) {
        try {
            sendMessage2Server(roomName, WebRTCConstants.CREATE_OR_JOIN_ROOM, new JSONObject());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void sendData(String peerId, DataChannel.Buffer buffer) {
        Peer peer = peers.get(peerId);
        if (peer != null) {
            peer.dataChannel.send(buffer);
        } else {
            Log.d(WebRTCConstants.LOG_TAG, "Peer id is not exists");
        }
    }

    public void receiveVideoCall(String peerId) {
        Peer peer = peers.get(peerId);
        if (peer != null) {
            peer.pc.addStream(localMS);
        } else {
            Log.d(WebRTCConstants.LOG_TAG, "Peer id is not exists");
        }
    }

    public void sendMessage2Server(String to, String type, JSONObject payload) throws JSONException {
        JSONObject message = new JSONObject();
        message.put("from", UsersObjectsManager.getInstance().getUser().userName);
        message.put("to", to);
        message.put("type", type);
        message.put("payload", payload);
        //Log.d(WebRTCConstants.LOG_TAG + type, message.toString());
        socketClient.emit("message", message);
    }

    //------------>PEER
    private class Peer implements PeerConnection.Observer, SdpObserver {
        private String id;
        private PeerConnection pc;
        private DataChannel dataChannel;

        public Peer (String id) {
            this.id = id;
            this.pc = pcFactory.createPeerConnection(iceServers, pcConstraints, this);
            this.dataChannel = pc.createDataChannel("DataChannel-" + id, new DataChannel.Init());

            setMedia();
            this.pc.addStream(localMS);
        }

        @Override
        public void onSignalingChange(PeerConnection.SignalingState signalingState) {
            Log.d(WebRTCConstants.LOG_TAG, id + "-Signaling change: " + signalingState.toString());
        }

        @Override
        public void onIceConnectionChange(PeerConnection.IceConnectionState iceConnectionState) {
            Log.d(WebRTCConstants.LOG_TAG, id + "-On ice connection change: " + iceConnectionState.toString());
            if (iceConnectionState == PeerConnection.IceConnectionState.DISCONNECTED) {
                removePeer(id);
            }
        }

        @Override
        public void onIceConnectionReceivingChange(boolean b) {
            Log.d(WebRTCConstants.LOG_TAG, id + "-On ice connection receiving change: " + b);
        }

        @Override
        public void onIceGatheringChange(PeerConnection.IceGatheringState iceGatheringState) {
            Log.d(WebRTCConstants.LOG_TAG, id + "-On ice gathering change: " + iceGatheringState.toString());
        }

        @Override
        public void onIceCandidate(IceCandidate candidate) {
            try {
                JSONObject payload = new JSONObject();
                payload.put("index", candidate.sdpMLineIndex);
                payload.put("id", candidate.sdpMid);
                payload.put("sdp", candidate.sdp);
                sendMessage2Server(id, "candidate", payload);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onAddStream(MediaStream mediaStream) {
            Log.d(WebRTCConstants.LOG_TAG, id + "-On add stream: " + mediaStream.toString());
            mListener.onRemoteStream(mediaStream, this.id);
        }

        @Override
        public void onRemoveStream(MediaStream mediaStream) {
            Log.d(WebRTCConstants.LOG_TAG, id + "-On remove stream");
        }

        @Override
        public void onDataChannel(DataChannel dataChannel) {
            Log.d(WebRTCConstants.LOG_TAG, id + "-On data channel" + dataChannel.label());
            MyDataObserver myDataObserver = new MyDataObserver(this.id);
            dataChannel.registerObserver(myDataObserver);
        }

        @Override
        public void onRenegotiationNeeded() {
           Log.d(WebRTCConstants.LOG_TAG, id + "-On renegotiation needed");
        }

        //SDP Observer
        public boolean local = false;
        @Override
        public void onCreateSuccess(SessionDescription sdp) {
            try {
                JSONObject payload = new JSONObject();
                payload.put("type", sdp.type.canonicalForm());
                payload.put("sdp", sdp.description);
                sendMessage2Server(id, sdp.type.canonicalForm(), payload);
                local = true;
                pc.setLocalDescription(Peer.this, sdp);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onSetSuccess() {
            Log.d(WebRTCConstants.LOG_TAG, id + "-On set sdp successful");
            if (!local)
                mListener.onSetRemoteSdpSuccessful(id);
            else
                local = false;
        }

        @Override
        public void onCreateFailure(String s) {
           Log.d(WebRTCConstants.LOG_TAG, id + "-On create sdp fail: " + s);
        }

        @Override
        public void onSetFailure(String s) {
           Log.d(WebRTCConstants.LOG_TAG, id + "-On set sdp fail");
        }
    }

    private void setMedia() {
        localMS = pcFactory.createLocalMediaStream(UsersObjectsManager.getInstance().getUser().userName + "-MEDIA_STREAM");
        if (WebRTCConfig.videoEnabled) {
            //Create video input operations
            String frontFacingCam = VideoCapturerAndroid.getNameOfFrontFacingDevice();
            VideoCapturer videoCapturer = VideoCapturerAndroid.create(frontFacingCam);
            MediaConstraints videoConstraints = new MediaConstraints();
            videoSource = pcFactory.createVideoSource(videoCapturer, videoConstraints);
            videoTrack = pcFactory.createVideoTrack(UsersObjectsManager.getInstance().getUser().userName + "-VIDEO_TRACK", videoSource);
            localMS.addTrack(videoTrack);
        }

        if (WebRTCConfig.audioEnabled) {
            MediaConstraints audioConstraints = new MediaConstraints();
            audioSource = pcFactory.createAudioSource(audioConstraints);
            audioTrack = pcFactory.createAudioTrack(UsersObjectsManager.getInstance().getUser().userName + "-AUDIO_TRACK", audioSource);
            localMS.addTrack(audioTrack);
        }
    }

    //AUDIO CONTROLS
    public boolean audioEnabled() {
        return audioTrack.enabled();
    }

    public void resumeAudioInput() {
        audioTrack.setEnabled(true);
    }

    public void pauseAudioInput() {
        audioTrack.setEnabled(false);
    }

    private void addIceServer() {
        iceServers.add(new PeerConnection.IceServer("stun:stun.l.google.com:19302"));
        iceServers.add(new PeerConnection.IceServer("stun:stun1.l.google.com:19302"));
        iceServers.add(new PeerConnection.IceServer("stun:stun2.l.google.com:19302"));
        iceServers.add(new PeerConnection.IceServer("stun:stun3.l.google.com:19302"));
        iceServers.add(new PeerConnection.IceServer("stun:stun4.l.google.com:19302"));
    }
}