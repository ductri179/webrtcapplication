package cntn2012.fit.hcmus.webrtcapplication.Src.Global;

/**
 * Created by NQH on 3/30/2016.
 */
public class Global {
     public static final String SIGNALING_SERVER_HOST = "https://ductriserver.herokuapp.com/";
     public static final String SERVER_HOST = "https://webrtcmongodb.herokuapp.com/";
     //public static final String SERVER_HOST = "http://192.168.14.114:1000";
//     public static final String SERVER_HOST =  "http://192.168.43.4:1000";
     public static final String SOCKET_EVENT_LOGIN = "login";
     public static final String SOCKET_EVENT_REGISTER = "register";

     public static final String SOCKET_EVENT_GET_FRIEND_LIST = "friend_list";
     public static final String SOCKET_EVENT_MESSAGE_LISTENER= "socket_message";
     public static final String SOCKET_EVENT_FRIEND_LISTENER= "friend_message";

     public static final String SOCKET_EVENT_PRIVATE_CHAT= "private_message";
     public static final String SOCKET_MESSAGE_RESPONSE_FRIEND_REQUEST= "response_friend_request";
     public static final String SOCKET_MESSAGE_FRIEND_REQUEST= "friend_request";
}
