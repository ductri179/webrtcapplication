var http = require('http');
var socketIO = require('socket.io');
var ip = process.env.IP;
var port = Number(process.env.PORT || 2013);

var server = http.createServer();
server.listen(port, function(){
	console.log('Server has started at localhost:%s', port);
});

var io = socketIO.listen(server);
io.set('match origin protocol', true);
io.set('origins', '*:*');
io.set('log level', 1);

var clients = [];

io.sockets.on('connection', function(socket) {
  
	console.log('A user has connected!');

	socket.emit('connected', 'Connect successful!');
	socket.on('register', function(data) {
		console.log('User registered: ' + data.userId);
		var clientInfo = new Object();
		clientInfo.userId = data.userId;
		clientInfo.socket = socket;
		clients.push(clientInfo);
		console.log('Client size: ' + clients.length);
	});

	socket.on('message', function(data) {
		//console.log('message: ' + data.type);
		if (data.type == 'offer' || data.type == 'candidate' || data.type == 'answer') {
			for (var i = 0; i < clients.length; ++i) {
				if (clients[i].userId == data.to) {
					var sendData = new Object();
					sendData.from = data.from;
					sendData.type = data.type;
					sendData.payload = data.payload;
					clients[i].socket.emit('message', sendData);
				}
			}
		}
		if (data.type != 'candidate') {
			console.log('Message type: ' + data.type + ' From: ' + data.from);
		}
	});
	
	socket.on('disconnect', function() {
		console.log("A user disconnected!")
		clients.splice(clients.indexOf(socket), 1);
		console.log('Client size: ' + clients.length);
	});
});