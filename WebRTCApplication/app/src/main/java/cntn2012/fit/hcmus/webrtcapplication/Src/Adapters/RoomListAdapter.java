package cntn2012.fit.hcmus.webrtcapplication.Src.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cntn2012.fit.hcmus.webrtcapplication.Activities.ChatActivity;
import cntn2012.fit.hcmus.webrtcapplication.R;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.Room;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.User;

/**
 * Created by NQH on 4/23/2016.
 */
public class RoomListAdapter extends ArrayAdapter implements View.OnClickListener{
    Context context;
    List<Room> listRoom;
    public RoomListAdapter(Context context, int resource, List<Room> listRoom) {
        super(context, resource, listRoom);
        this.context = context;
        this.listRoom = listRoom;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        User fr = listRoom.get(position).getUser();


        View rowView = inflater.inflate(R.layout.list_item_friend, parent, false);
        TextView tvUserName = (TextView) rowView.findViewById(R.id.tvUserName);
        ImageView iv = (ImageView) rowView.findViewById(R.id.imgUserAvatar);

        tvUserName.setText(fr.userName);
        rowView.setTag(position);
        rowView.setOnClickListener(this);
        return rowView;
    }

    @Override
    public void onClick(View v) {
        User user = ((Room)listRoom.get((int)v.getTag())).getUser();
        Intent intent = new Intent(context, ChatActivity.class);

        String friendName = user.userName;
        intent.putExtra("friendName", friendName);
        context.startActivity(intent);
    }


}
