package cntn2012.fit.hcmus.webrtcapplication.Activities.Tabs;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cntn2012.fit.hcmus.webrtcapplication.R;
import cntn2012.fit.hcmus.webrtcapplication.Src.Adapters.FriendListAdapter;
import cntn2012.fit.hcmus.webrtcapplication.Src.Adapters.FriendRequestListAdapter;
import cntn2012.fit.hcmus.webrtcapplication.Src.Global.Global;
import cntn2012.fit.hcmus.webrtcapplication.Src.IOSocket.SocketIOAccess;
import cntn2012.fit.hcmus.webrtcapplication.Src.IOSocket.SocketIOMessage;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.FriendRequest;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.User;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.UsersObjectsManager;

public class FriendListFragment extends Fragment implements View.OnClickListener {

    EditText etAddFriend;
    Button btnAddFriend;
    ListView lvFriendRequest;
    ListView lvFriendList;
    ArrayList<User> friendList;
    ArrayAdapter friendRequestAdapter;
    ArrayAdapter friendListAdapter;
    SocketIOAccess socket;
    Toast toast;
    UsersObjectsManager uoManager= UsersObjectsManager.getInstance();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        socket=SocketIOAccess.getInstance();
        socket.on(Global.SOCKET_EVENT_FRIEND_LISTENER, OnFriendListListener);
        toast= Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friend_list, container, false);
        connectView(view);
        showFriendRequest();
        showFriendList();
        return view;
    }

    private Emitter.Listener OnFriendListListener = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String messageType;
                    String from;
                    try {
                        messageType=data.getString("type");
                        from=data.getString("from");

                    } catch (JSONException e) {
                        messageType="";
                        from="";
                    }
                    switch(messageType)
                    {
                        case Global.SOCKET_MESSAGE_FRIEND_REQUEST:
                            toast.setText("friend request: " + from);
                            toast.show();
                            FriendRequest fr =new FriendRequest(from);
                            uoManager.addFriendRequest(fr);
                            friendRequestAdapter.notifyDataSetChanged();
                            break;

                        case Global.SOCKET_EVENT_GET_FRIEND_LIST:
                            try {
                                JSONObject friend= data.getJSONObject("data");
                                User user=new User();
                                user.userName= friend.getString("username");
                                friendList.add(user);
                                friendListAdapter.notifyDataSetChanged();
                            } catch (JSONException e) {

                            }
                            break;

                        default:
                            break;
                    }
                }
            });
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        friendRequestAdapter.notifyDataSetChanged();
    }

    private void connectView(View view){
        etAddFriend= (EditText) view.findViewById(R.id.etAddFriend);
        lvFriendList=(ListView) view.findViewById(R.id.lvFriendList);
        lvFriendRequest=(ListView) view.findViewById(R.id.lvFriendRequest);
        btnAddFriend=(Button) view.findViewById(R.id.btnAddFriend);
        btnAddFriend.setOnClickListener(this);
    }

    public void showFriendRequest(){
        List<FriendRequest> frList= uoManager.getFrList();
        friendRequestAdapter= new FriendRequestListAdapter(getActivity(),0,frList);
        lvFriendRequest.setAdapter(friendRequestAdapter);
    }

    private void showFriendList() {
        friendList= new ArrayList<>();
        friendListAdapter= new FriendListAdapter(getActivity(),0,friendList);
        lvFriendList.setAdapter(friendListAdapter);

    }
    @Override
    public void onClick(View v) {
        if(v==btnAddFriend){
            String username=etAddFriend.getText().toString();
            SocketIOMessage msg = new SocketIOMessage(Global.SOCKET_MESSAGE_FRIEND_REQUEST, uoManager.getUser().userName, username);
            socket.emitMessage(msg);
        }
    }
}
