package cntn2012.fit.hcmus.webrtcapplication.Activities.Tabs;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.List;

import cntn2012.fit.hcmus.webrtcapplication.R;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.PublicRoom;

public class RoomFragment extends Fragment {

    Button btnCreateNewRoom;
    EditText etRoomTitle;
    ListView lvPublicRoom;
    List<PublicRoom> listPublicRoom;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        connectView();
    }

    private void connectView() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_room, container, false);
    }
}
