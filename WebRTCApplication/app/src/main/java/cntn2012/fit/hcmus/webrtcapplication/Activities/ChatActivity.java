package cntn2012.fit.hcmus.webrtcapplication.Activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.Layout;
import android.text.TextWatcher;
import android.text.format.Time;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.kennyc.bottomsheet.BottomSheet;
import com.kennyc.bottomsheet.BottomSheetListener;

import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.DataChannel;
import org.webrtc.MediaStream;

import java.util.List;

import cntn2012.fit.hcmus.webrtcapplication.R;
import cntn2012.fit.hcmus.webrtcapplication.Src.Adapters.ChatListAdapter;
import cntn2012.fit.hcmus.webrtcapplication.Src.Global.Global;
import cntn2012.fit.hcmus.webrtcapplication.Src.IOSocket.SocketIOAccess;
import cntn2012.fit.hcmus.webrtcapplication.Src.IOSocket.SocketIOMessage;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.Message;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.Room;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.RoomManager;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.User;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.UsersObjectsManager;
import cntn2012.fit.hcmus.webrtcapplication.Src.WebRTC.WebRTCClient;
import cntn2012.fit.hcmus.webrtcapplication.Src.WebRTC.WebRTCConfig;

public class ChatActivity extends Activity {

    ListView listView;
    ChatListAdapter adapter;
    List<Message> msgs;
    EditText etMsg;
    ImageButton btnAddSend;
    ImageButton btnRecord;
    boolean isSend = false;

    SocketIOAccess socket= SocketIOAccess.getInstance();
    UsersObjectsManager uoManager= UsersObjectsManager.getInstance();

    User user;
    User peerUser;
    Room room;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        user = uoManager.getUser();
        peerUser= new User();
        peerUser.userName= getIntent().getExtras().getString("friendName");

        room= RoomManager.getInstance().getRoom(peerUser.userName);

        loadAllControls();

        msgs = room.getMsgList();
        adapter = new ChatListAdapter(this, 0, msgs);//dump
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        socket.on("private_message", onPrivateChatListener);

        WebRTCConfig.audioEnabled = true;
        WebRTCConfig.client = new WebRTCClient(Global.SIGNALING_SERVER_HOST, this);
        WebRTCConfig.client.registerToSignalingServer(UsersObjectsManager.getInstance().getUser().userName);

        String roomName = "123";
        WebRTCConfig.client.joinRoom(roomName);

        ChatListener chatListener = new ChatListener();
        WebRTCConfig.client.setListener(chatListener);

        setEditTextModifyEvent();
    }

    private void setAddMenu() {
        BottomSheetListener addingMenuListener = new BottomSheetListener() {
            @Override
            public void onSheetShown() {

            }

            @Override
            public void onSheetItemSelected(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.writeNewQuestion : {
                        Button btnShowMenu = (Button) findViewById(R.id.btnShowContextMenu);
                        btnShowMenu.performLongClick();
                        break;
                    }
                }
            }

            @Override
            public void onSheetDismissed(int i) {

            }
        };
        new BottomSheet.Builder(ChatActivity.this)
                .setSheet(R.menu.menu_add_chat)
                .setTitle("Options")
                .setListener(addingMenuListener)
                .grid()
                .setColumnCount(3)
                .show();
    }

    private void loadAllControls() {
        listView = (ListView) findViewById(R.id.lvPrivateChat);
        btnAddSend = (ImageButton) findViewById(R.id.btnAddSend);
        btnRecord = (ImageButton) findViewById(R.id.btnRecord);
        etMsg = (EditText) findViewById(R.id.etEnterMessage);
        Button btnShowMenu = (Button) findViewById(R.id.btnShowContextMenu);
        registerForContextMenu(btnShowMenu);
    }

    private void setEditTextModifyEvent() {
        etMsg = (EditText) findViewById(R.id.etEnterMessage);
        etMsg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (etMsg.getText().length() > 0) {
                    btnAddSend.setImageResource(R.drawable.send_message);
                    isSend = true;
                } else {
                    btnAddSend.setImageResource(R.drawable.add);
                    isSend = false;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    public void sendAddSelection(View v) {
        if (isSend) {
            sendMessage(v);
        } else {
            setAddMenu();
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Choose question type");
        menu.add(0, 0, 0, "Multiple choise");
        menu.add(0, 1, 0, "Fill in blank");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0 : {
                final LinearLayout layout = (LinearLayout) findViewById(R.id.addQuestionLayout);
                layout.removeAllViews();
                LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                layout.addView(layoutInflater.inflate(R.layout.layout_mutiple_choise, (ViewGroup)findViewById(R.id.layoutParent), false));

                Button btnCancel = (Button) layout.findViewById(R.id.btnMulCancel);
                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        layout.removeAllViews();
                    }
                });

                break;
            }
        }
        return super.onContextItemSelected(item);
    }

    public void sendMessage(View v) {
        Time now = new Time();
        now.setToNow();
        String time = now.hour + ":" + now.minute;

        //socket Message
        etMsg = (EditText) findViewById(R.id.etEnterMessage);
        SocketIOMessage sMsg= new SocketIOMessage("private_message", user.userName, peerUser.userName);
        sMsg.addArgument("content",etMsg.getText().toString());
        sMsg.addArgument("time", time);
        socket.emitMessage(sMsg);

        //Create display message
        String msgStr = ((EditText)findViewById(R.id.etEnterMessage)).getText().toString();

        Message msg = new Message(msgStr, time, Message.Type.RIGHT);
        msgs.add(msg);
        //update adapter
        adapter.notifyDataSetChanged();

        //reset enter chat edit text
        etMsg.setText("");
    }


    private Emitter.Listener onPrivateChatListener = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String messageType;
                    String from;
                    try {
                        messageType=data.getString("type");
                        from=data.getString("from");
                    } catch (JSONException e) {
                        messageType="";
                        from="";
                    }
                    if(room.getUser().userName.equals(from))
                    switch(messageType)
                    {
                        case "private_message":
                                adapter.notifyDataSetChanged();
                            break;

                        default:
                            break;
                    }
                }
            });
        }
    };

    private class ChatListener implements WebRTCClient.RtcListener {

        @Override
        public void onLocalStream(MediaStream localStream) { }

        @Override
        public void onRemoteStream(MediaStream remoteStream, String peerId) {}

        @Override
        public void onOffer(final String from) {
            ChatActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(ChatActivity.this, "Offer from: " + from, Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onReceiveMessage(DataChannel.Buffer buffer, String id) {}

        @Override
        public void onNewUserJoin(final String userName) {
            ChatActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(ChatActivity.this, "New user: " + userName, Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onSetRemoteSdpSuccessful(String userName) {
            WebRTCConfig.client.makeAnswer(userName);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //WebRTCConfig.client.disconnect();
    }
}