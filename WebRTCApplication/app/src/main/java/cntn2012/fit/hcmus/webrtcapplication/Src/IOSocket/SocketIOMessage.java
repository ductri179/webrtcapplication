package cntn2012.fit.hcmus.webrtcapplication.Src.IOSocket;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by NQH on 3/30/2016.
 */
public class SocketIOMessage {

    private JSONObject from;
    private JSONObject to;
    private String event;
    JSONObject args;

    public SocketIOMessage(String event,String fromUser, String to) {
        this.from = new JSONObject();
        try {
            this.from.put("username", fromUser);
        } catch (JSONException e) {
            try {
                this.from.put("username", "");
            } catch (JSONException e1) {
            }
        }
        this.to = new JSONObject();
        try {
            this.to.put("username",to);
        } catch (JSONException e) {
            try {
                this.to.put("username", "");
            } catch (JSONException e1) {
            }
        }
        this.event = event;
        args = new JSONObject();
    }

    //============Getter and setter=====
    public JSONObject getFrom() {
        return from;
    }
    public void setFrom(String from){

            this.from.remove("username");
        try {
            this.from.put("username", from);
        }catch(JSONException e){
            try {
                this.from.put("username", "");
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
        }
    }

    public JSONObject getTo() {
        return to;
    }
    public void setTo(String to){
        this.from.remove("username");
        try {
            this.from.put("username", to);
        } catch (JSONException e) {
            try {
                this.from.put("username", "");
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
        }
    }

    public String getEvent() {
        return event;
    }
    public void setEvent(String event) {
        this.event = event;
    }

    public JSONObject getArgs() {
        return args;
    }
    public void setArgs(JSONObject args) {
        this.args = args;
    }
//==========================

    public void addArgument(String name, Object arg){
        try {
            args.put(name, arg);
        }catch(JSONException e){
            return;
        }
    }


}
