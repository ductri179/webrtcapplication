package cntn2012.fit.hcmus.webrtcapplication.Src.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cntn2012.fit.hcmus.webrtcapplication.Activities.ChatActivity;
import cntn2012.fit.hcmus.webrtcapplication.R;
import cntn2012.fit.hcmus.webrtcapplication.Src.Global.Global;
import cntn2012.fit.hcmus.webrtcapplication.Src.IOSocket.SocketIOAccess;
import cntn2012.fit.hcmus.webrtcapplication.Src.IOSocket.SocketIOMessage;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.FriendRequest;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.Room;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.RoomManager;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.User;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.UsersObjectsManager;

/**
 * Created by NQH on 4/23/2016.
 */
public class FriendListAdapter extends ArrayAdapter implements View.OnClickListener{
    Context context;
    List<User> friendList;
    RoomManager roomManager= RoomManager.getInstance();
    Button btnAccept;
    Button btnDecline;
    public FriendListAdapter(Context context, int resource, List<User> friendList) {
        super(context, resource, friendList);
        this.context = context;
        this.friendList = friendList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        User fr = friendList.get(position);


        View rowView = inflater.inflate(R.layout.list_item_friend, parent, false);
        TextView tvUserName = (TextView) rowView.findViewById(R.id.tvUserName);
        ImageView iv = (ImageView) rowView.findViewById(R.id.imgUserAvatar);

        tvUserName.setText(fr.userName);
        rowView.setTag(position);
        rowView.setOnClickListener(this);
        return rowView;
    }

    @Override
    public void onClick(View v) {
        User user = friendList.get((int)v.getTag());
        Intent intent = new Intent(context, ChatActivity.class);
        String friendName = user.userName;
        RoomManager.getInstance().getRoom(friendName);
        intent.putExtra("friendName", friendName);
        context.startActivity(intent);
    }


}
