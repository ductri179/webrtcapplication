package cntn2012.fit.hcmus.webrtcapplication.Activities.Tabs;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cntn2012.fit.hcmus.webrtcapplication.Activities.ChatActivity;
import cntn2012.fit.hcmus.webrtcapplication.R;
import cntn2012.fit.hcmus.webrtcapplication.Src.Adapters.RoomListAdapter;
import cntn2012.fit.hcmus.webrtcapplication.Src.Global.Global;
import cntn2012.fit.hcmus.webrtcapplication.Src.IOSocket.SocketIOAccess;
import cntn2012.fit.hcmus.webrtcapplication.Src.IOSocket.SocketIOMessage;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.FriendRequest;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.Message;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.Room;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.RoomManager;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.User;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.UsersObjectsManager;

public class HomeFragment extends Fragment {

    RoomManager roomManager;
    UsersObjectsManager uoManager;
    SocketIOAccess socket;
    RoomListAdapter roomListAdapters;
    List<Room> listRoom;
    List<Message> listMsg;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        roomManager = RoomManager.getInstance();
        uoManager= UsersObjectsManager.getInstance();
        listRoom=roomManager.getListRoom();
        roomListAdapters = new RoomListAdapter(getActivity(),0,listRoom);


        socket =SocketIOAccess.getInstance();
        socket.on("private_message", onHomeListener);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ListView lvRoomList = (ListView)view.findViewById(R.id.lvRoomList);
        lvRoomList.setAdapter(roomListAdapters);

        Button btnStartChat = (Button) view.findViewById(R.id.btnStartChat);
        btnStartChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("ductri", "adfadf");
                Intent intent = new Intent(getActivity(), ChatActivity.class);
                String friendName = "dfaf";
                intent.putExtra("friendName", friendName);
                startActivity(intent);
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        roomListAdapters.notifyDataSetChanged();
    }

    private Emitter.Listener onHomeListener = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String messageType;
                    String from;
                    try {
                        messageType = data.getString("type");
                        from = data.getString("from");
                    } catch (JSONException e) {
                        messageType = "";
                        from = "";
                    }
                    switch (messageType) {
                        case Global.SOCKET_EVENT_PRIVATE_CHAT:
                            // toast.setText("friend request: " + from);
                            //toast.show();
                            Room room = roomManager.getRoom(from);
                            listMsg=room.getMsgList();
                            roomListAdapters.notifyDataSetChanged();
                            //startRoom(room);
                                try {
                                    //Create message
                                    String time = data.getString("time");
                                    String msgStr= data.getString("content");
                                    Log.d("tag1", msgStr);
                                    final Message msg = new Message(msgStr, time, Message.Type.LEFT);
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            listMsg.add(msg);
                                        }
                                    });
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                break;

                            default:
                                break;

                    }
                }
            });
        }
    };
}
