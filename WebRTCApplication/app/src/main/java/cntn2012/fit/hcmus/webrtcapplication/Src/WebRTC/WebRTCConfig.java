package cntn2012.fit.hcmus.webrtcapplication.Src.WebRTC;

/**
 * Created by Oscar on 3/29/2016.
 */
public class WebRTCConfig {

    public static WebRTCClient client;

    public static final int MAX_CLIENTS = 100;

    public static boolean videoEnabled = false;
    public static boolean audioEnabled = true;
}
