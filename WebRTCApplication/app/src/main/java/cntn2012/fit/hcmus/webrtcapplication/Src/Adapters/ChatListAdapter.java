package cntn2012.fit.hcmus.webrtcapplication.Src.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.Message;
import cntn2012.fit.hcmus.webrtcapplication.R;

/**
 * Created by Oscar on 3/29/2016.
 */
public class ChatListAdapter extends ArrayAdapter<Message> {
    Context context;
    List<Message> msgs;

    public ChatListAdapter(Context context, int resource, List<Message> msgs) {
        super(context, resource, msgs);
        this.context = context;
        this.msgs = msgs;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = null;
        if (msgs.get(position).getType() == Message.Type.LEFT) {
            rowView = inflater.inflate(R.layout.chat_bubble_left, parent, false);
            TextView tvMsg = (TextView) rowView.findViewById(R.id.tvChatMessageLeft);
            TextView tvTime = (TextView) rowView.findViewById(R.id.tvChatTimeLeft);
            ImageView imgUser = (ImageView) rowView.findViewById(R.id.imgUserChatLeft);

            tvMsg.setText(msgs.get(position).getMsg());
            tvTime.setText(msgs.get(position).getTimeStamp());
            imgUser.setImageResource(R.drawable.android_icon);
        } else if (msgs.get(position).getType() == Message.Type.RIGHT) {
            rowView = inflater.inflate(R.layout.chat_bubble_right, parent, false);
            TextView tvMsg = (TextView) rowView.findViewById(R.id.tvChatMessageRight);
            TextView tvTime = (TextView) rowView.findViewById(R.id.tvChatTimeRight);
            ImageView imgUser = (ImageView) rowView.findViewById(R.id.imgUserChatRight);

            tvMsg.setText(msgs.get(position).getMsg());
            tvTime.setText(msgs.get(position).getTimeStamp());
            imgUser.setImageResource(R.drawable.android_icon);
        }

        return rowView;
    }
}
