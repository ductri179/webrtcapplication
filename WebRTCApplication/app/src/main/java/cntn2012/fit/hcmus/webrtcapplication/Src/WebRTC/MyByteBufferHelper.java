package cntn2012.fit.hcmus.webrtcapplication.Src.WebRTC;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;

/**
 * Created by Oscar on 3/28/2016.
 */
public class MyByteBufferHelper {
    public static Charset charset = Charset.forName("UTF-8");
    public static CharsetEncoder encoder = charset.newEncoder();
    public static ByteBuffer string2ByteBuffer(String str) {
        try{
            return encoder.encode(CharBuffer.wrap(str));
        }catch(Exception e){e.printStackTrace();}
        return null;
    }
}
