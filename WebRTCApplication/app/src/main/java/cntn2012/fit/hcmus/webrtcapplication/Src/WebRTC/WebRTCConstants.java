package cntn2012.fit.hcmus.webrtcapplication.Src.WebRTC;

/**
 * Created by Oscar on 3/29/2016.
 */
public class WebRTCConstants {
    //public static final String SIGNALING_SERVER_HOST = "https://ductriserver.herokuapp.com/";
    public static final String LOG_TAG = "ductri";
    public static final String CREATE_OR_JOIN_ROOM = "createOrJoinRoom";
    public static final String NEW_USER_JOIN_ROOM = "newUserJoinRoom";
    public static final String MESSAGE = "message";
    public static final String OFFER = "offer";
    public static final String CANDIDATE = "candidate";
    public static final String ANSWER = "answer";
}
