package cntn2012.fit.hcmus.webrtcapplication.Src.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.nkzawa.emitter.Emitter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cntn2012.fit.hcmus.webrtcapplication.Activities.ChatActivity;
import cntn2012.fit.hcmus.webrtcapplication.R;
import cntn2012.fit.hcmus.webrtcapplication.Src.Global.Global;
import cntn2012.fit.hcmus.webrtcapplication.Src.IOSocket.SocketIOAccess;
import cntn2012.fit.hcmus.webrtcapplication.Src.IOSocket.SocketIOMessage;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.FriendRequest;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.PublicRoom;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.Room;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.User;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.UsersObjectsManager;

/**
 * Created by NQH on 4/23/2016.
 */
public class ListPublicRoomAdapter extends ArrayAdapter implements View.OnClickListener{
    Context context;
    List<PublicRoom> listRoom;
    SocketIOAccess socket= SocketIOAccess.getInstance();
    public ListPublicRoomAdapter(Context context, int resource, List<PublicRoom> listRoom) {
        super(context, resource, listRoom);
        this.context = context;
        this.listRoom = listRoom;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        PublicRoom pr = listRoom.get(position);


        View rowView = inflater.inflate(R.layout.list_item_public_room, parent, false);
        TextView tvUserName = (TextView) rowView.findViewById(R.id.tvPublicRoomTittle);
        ImageView iv = (ImageView) rowView.findViewById(R.id.imgUserAvatar);

        tvUserName.setText(pr.getRoomTitle());
        rowView.setTag(position);
        rowView.setOnClickListener(this);
        return rowView;
    }

    @Override
    public void onClick(View v) {
        PublicRoom pr= listRoom.get((int)v.getTag());
        User user= UsersObjectsManager.getInstance().getUser();
        SocketIOMessage msg= new SocketIOMessage("join_public_room", user.userName, pr.getRoomTitle());
        socket.on("join_public_room", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                String messageType;
                String from;
                try {
                    messageType=data.getString("type");
                    from=data.getString("from");
                } catch (JSONException e) {
                    messageType="";
                    from="";
                }
                switch(messageType)
                {
                    case "join_public_room":
                        //...
                        break;

                    default:
                        break;
                }
            }
        });

        if(pr.addMember(UsersObjectsManager.getInstance().getUser())){
            Intent intent = new Intent(context, ChatActivity.class);
            String friendName = user.userName;
            intent.putExtra("friendName", friendName);
            context.startActivity(intent);
        }

    }


}
