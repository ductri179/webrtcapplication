package cntn2012.fit.hcmus.webrtcapplication.Src.Objects;

/**
 * Created by NQH on 4/17/2016.
 */
public class FriendRequest {

    private String from;
    private boolean response;
    public FriendRequest(String from){
        this.from=from;
        response=false;
    }

    public String getFrom() {
        return from;
    }

    public void setResponse(boolean response) {
        this.response = response;
    }


}
