package cntn2012.fit.hcmus.webrtcapplication.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;

import cntn2012.fit.hcmus.webrtcapplication.R;
import cntn2012.fit.hcmus.webrtcapplication.Src.Functions.UsernameAndPasswordChecker;
import cntn2012.fit.hcmus.webrtcapplication.Src.Global.Global;
import cntn2012.fit.hcmus.webrtcapplication.Src.IOSocket.SocketIOAccess;
import cntn2012.fit.hcmus.webrtcapplication.Src.IOSocket.SocketIOMessage;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.User;
import cntn2012.fit.hcmus.webrtcapplication.Src.Objects.UsersObjectsManager;

public class LoginActivity extends Activity implements View.OnClickListener {

    SocketIOAccess socket;
    Button btnLogin;
    TextView tvRegister;
    TextView tvForgotPassword;
    EditText etUserName;
    EditText etPassword;

    Toast toast;
    UsernameAndPasswordChecker stringChecker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        connectView();
        toast= Toast.makeText(getBaseContext(),"", Toast.LENGTH_SHORT);
        stringChecker=new UsernameAndPasswordChecker();
        //Connect socketio server
        //socket= SocketIOAccess.getInstance();
        socket=SocketIOAccess.getInstance();
        socket.connect();
        setEvent();
    }


    public void connectView()
    {
        btnLogin=(Button)findViewById(R.id.btnLogIn);
        tvRegister=(TextView)findViewById(R.id.tvRegister);
        tvForgotPassword=(TextView)findViewById(R.id.tvForgotPassword);
        etUserName=(EditText)findViewById(R.id.etLogInUserName);
        etPassword=(EditText)findViewById(R.id.etLogInPassword);
    }
    public void setEvent(){
        btnLogin.setOnClickListener(this);
        tvRegister.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if(v==btnLogin) {
            login();
        }
        else if(v==tvRegister){
            openSignuprActivity();
        }

    }


    public void login(){
        String strUser=etUserName.getText().toString();
        String strPassword=etPassword.getText().toString();
        if(stringChecker.checkValidString(strUser)==null && stringChecker.checkValidString(strPassword)==null) {
            SocketIOMessage msg = new SocketIOMessage(Global.SOCKET_EVENT_LOGIN, "", "");
            msg.addArgument("username", strUser);
            msg.addArgument("password", strPassword);
            socket.emitMessage(msg);
            socket.on(Global.SOCKET_EVENT_LOGIN, onLoginListener);
        }else{
            toast.setText(R.string.LoginError1);
            toast.show();
        }
    }
    private Emitter.Listener onLoginListener = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    int result  = (int) args[0];
                    switch(result)
                    {
                        case 0:
                            toast.setText(R.string.LoginError2);
                            toast.show();
                            break;
                        case 1:
                            toast.setText(R.string.LoginString1);
                            toast.show();
                            socket.off("new message", onLoginListener);
                            User user=new User();
                            user.userName=etUserName.getText().toString();
                            UsersObjectsManager uoManager= new UsersObjectsManager(user);
                            goMainActivity();
                            break;
                        default:
                            toast.setText(R.string.LoginError1);
                            toast.show();
                            break;
                    }
                }
            });
        }
    };

    public void openSignuprActivity(){
        Intent intent=new Intent(this,SignUpActivity.class);

        startActivity(intent);
    }
    public void goMainActivity(){
        Intent intent=new Intent(this,MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
