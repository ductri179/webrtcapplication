package cntn2012.fit.hcmus.webrtcapplication.Src.Functions;

/**
 * Created by NQH on 3/31/2016.
 */

//Lớp kiểm tra chuỗi username và password
public class UsernameAndPasswordChecker {

    //Kiểm tra chuỗi username và password có hợp lệ hay không.
    //Nếu không hợp lệ trả về chuỗi err
    //Nếu hợp lệ thì chuỗi null
    public String checkValidString(String str) {
        String errMsg=null;
        boolean result=true;
        if(str==null) {
            errMsg= "string null";
        }else if(str.length()<4) {
            errMsg="Độ dài phải 4 ký tự trở lên";
        }else if(str.length()>10){
            errMsg="Độ dài quá 10 ký tự";
        }else{
            for(int i=0, length=str.length();i<length;++i) {
                char c = str.charAt(i);
                if (!Character.isLetterOrDigit(c) && c != '_') {
                    result = false;
                    errMsg = "Chỉ được chứa các ký tự, số và '_' ";
                }
            }
            if(result)
                if(Character.isDigit(str.charAt(0))){
                    errMsg="Không được bắt đầu bằng chữ số";
                }
        }
        return errMsg;
    }

}

