package cntn2012.fit.hcmus.webrtcapplication.Src.Objects;

/**
 * Created by Oscar on 3/29/2016.
 */
public class Message {

    public enum Type {
        LEFT, RIGHT
    }
    private User user;
    private String msg;
    private String timeStamp;
    private Type type;

    public Message(String msg, String timeStamp, Type type) {
        this.setMsg(msg);
        this.setTimeStamp(timeStamp);
        this.setType(type);
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
