package cntn2012.fit.hcmus.webrtcapplication.Src.IOSocket;


import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.io.Serializable;
import java.net.URISyntaxException;

import cntn2012.fit.hcmus.webrtcapplication.Src.Global.Global;

/**
 * Created by NQH on 3/30/2016.
 */

public class SocketIOAccess{

    //Signleton
    private SocketIOAccess(){
        try {
            mSocket = IO.socket(Global.SERVER_HOST);
        } catch (URISyntaxException e) {
            mSocket= null;
        }
    }
    private static SocketIOAccess INSTANCE=new SocketIOAccess();
    public static SocketIOAccess getInstance(){
        return INSTANCE;
    }


    private Socket mSocket;
    public boolean connect() {
        if(mSocket==null)
            return false;

        mSocket.connect();
        return true;
    }
    public boolean connected(){
        return mSocket.connected();
    }
    public void disconnect() {
        if (mSocket==null ||mSocket.connected())
            mSocket.disconnect();
    }


    public boolean emitMessage(SocketIOMessage msg) {
        if (mSocket==null || !mSocket.connected())
            return false;
        mSocket.emit(msg.getEvent(),msg.getFrom(), msg.getTo(), msg.getArgs());
        return true;
    }

    public boolean on(String event, Emitter.Listener listener){
        if(mSocket==null || !mSocket.connected())
            return false;

        mSocket.on(event, listener);
        return true;
    }

    public void off(String event, Emitter.Listener listener){
        if(mSocket==null || !mSocket.connected())
            mSocket.off(event, listener);
    }

}
