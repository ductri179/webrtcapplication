package cntn2012.fit.hcmus.webrtcapplication.Src.Objects;

import java.util.ArrayList;
import java.util.List;

import cntn2012.fit.hcmus.webrtcapplication.Src.Adapters.FriendListAdapter;
import cntn2012.fit.hcmus.webrtcapplication.Src.Adapters.FriendRequestListAdapter;

/**
 * Created by NQH on 4/17/2016.
 */
public class UsersObjectsManager {

    private User user;
    private List<FriendRequest> frList;

    public List<User> getFriendList() {
        return friendList;
    }

    private List<User> friendList;


    private static UsersObjectsManager instance=null;

    public UsersObjectsManager(User user){
        this.user=user;
        frList=new ArrayList<>();
        friendList= new ArrayList<>();
        instance=this;
    }
    public static UsersObjectsManager getInstance(){
        return instance;
    }


    public User getUser() {
        return user;
    }

    public List<FriendRequest> getFrList() {
        return frList;
    }
    public void addFriendRequest(FriendRequest fr){
        frList.add(fr);
    }
}
